var arrays = [[1, 2, 3], [4, 5], [6]];
function flattening(arrays) {
	return arrays.reduce(function(prevArray,curArray){
		return prevArray.concat(curArray);
	});
};
console.log(flattening(arrays));