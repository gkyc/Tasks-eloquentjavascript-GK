// Function helps computing average age in array
function average(array) {
    function plus(a, b) { return a + b; }
    return array.reduce(plus) / array.length;
};

// Create object with KEY-name(number of century e.g. 16) and VALUE-array of persons assigned to this century
function group(array) {
    var centuryGroup = {}
    array.forEach(function(entry){
        var groupName = Math.ceil(entry.died / 100);
        if (!Array.isArray(centuryGroup[groupName])) { centuryGroup[groupName] = []; }
        centuryGroup[groupName].push(entry);
    });
    return centuryGroup
};
// Make array with all objects - centuries from JSON file with data
var centuryMap = group(ancestry);

// For mapping based on age
function age(person){
    return person.died - person.born
};
// Compute average age in century
for(var index in centuryMap){
    var centuryAverage = average(centuryMap[index].map(age));
    console.log(index + ": " + centuryAverage);
};
// 16: 43.5
// 17: 51.2
// 18: 52.78947368421053
// 19: 54.833333333333336
// 20: 84.66666666666667
// 21: 94