// Functions helping filter through arrays
function average(array) {
    function plus(a, b) { return a + b; }
    return array.reduce(plus) / array.length;
};

var byName = {};
ancestry.forEach(function(person) {
    byName[person.name] = person;
});

function hasKnownMother(person){
    if(byName[person.mother] != null){return true};
};
function ageDiff(p) { return p.born - byName[p.mother].born; }

//Filtering and computing
var childsWithKnownMother = ancestry.filter(hasKnownMother);
var ageDiff = childsWithKnownMother.map(ageDiff);
var ageDiffAverage = average(ageDiff)

console.log(ageDiffAverage) // 31.2222222